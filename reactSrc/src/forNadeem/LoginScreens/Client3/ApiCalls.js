import axios from "axios";

export default {
	user : {
		login : credentials=> 
			axios.post("/forNadeem/client3Login", {credentials} ).then(res => res.data),
		getUserData : jwt=> 
			axios.post("/forNadeem/client3GetUserData", {jwt} ).then(res => res.data),
	}
}