import {CLIENT3_LOGGED_IN} from "./types";

export default function Client3ScreenUser(state = {}, action = {}) {
	switch (action.type) {
		case CLIENT3_LOGGED_IN:
			return action.user;
		default:
			return state
	}
}