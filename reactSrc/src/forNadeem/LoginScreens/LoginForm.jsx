import React from "react";

class LoginForm extends React.Component{
    state={
        data: {},
        ApiErrors: "",
        loading: false
    }
    onSubmit = (e) => {
        e.preventDefault();
        this.setState({loading: true})
        this.props.submit(this.state.data).then(() => {
            this.setState({ApiErrors:""});
            this.setState({loading: false})
        }).catch(res=>{
            this.setState({loading: false})
            this.props.errByApi(res);
        });
    }
    onChange = event => this.setState({
        data: {...this.state.data, [event.target.name] : event.target.value }
    });
    render(){
        const {field1, field2, cta} = this.props.formStruct;
        const {ApiErrors} = this.state;
        const {onSubmit, onChange} = this;
        return(
            <div>
                <form onSubmit={onSubmit}>
                    <div className="form-group">
                        <label htmlFor="firstField">{field1.label}</label>
                        <input
                            type={field1.type||'text'}
                            className="form-control"
                            required
                            autoSave="false"
                            id="firstField"
                            onChange={onChange}
                            name={field1.name || "field1"}
                            autoFocus={true}
                            placeholder={field1.placeholder}
                        />
                    </div><br/>
                    <div className="form-group">
                        <label htmlFor="secondField">{field2.label}</label>
                        <input
                            type={field2.type||'password'}
                            className="form-control"
                            required
                            id="secondField"
                            onChange={onChange}
                            autoSave="false"
                            name={field2.name || "field2"}
                            autoFocus={false}
                            placeholder={field2.placeholder}
                        />
                    </div><br/>
                    <input type="submit" value={cta.text} className={"btn py-0-5 btn"+cta.color} />
                </form>
                {this.state.loading && (
                    <div className="loader">
                        <div></div>
                        <h1>Please Wait...</h1>
                    </div>
                )}
            </div>
        );
    };
};

export default LoginForm;