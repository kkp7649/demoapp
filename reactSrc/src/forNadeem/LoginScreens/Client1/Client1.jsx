import React, {useState} from "react";
import {connect} from "react-redux";
import {login} from "./AuthAction";
import LoginForm from "../LoginForm";

const Client1 = (props) => {
    const [ApiError, setApiError] = useState("");
    const formStruct = {
        field1: {
            name: 'acname',
            label: 'Account Name: ',
            type: 'text',
            placeholder: 'Enter Account Name'
        },
        field2: {
            name: 'key',
            label: 'Key: ',
            type: 'password',
            placeholder: 'Enter the key'
        },
        cta: {
            text: 'Sign in',
            color: 'Green'
        }
    };
    const onSubmit = (data) => 
        props.login(data).then(msg => {
            setApiError("");
            console.log(msg)
            props.history.push("/formAssignment/client1/dashboard/")
        });
    
    const onErrByApi = (msg) => {
        console.log(msg.response.data)
        setApiError(msg.response.data.err.errors.global);
    }
    return(
        <div>
           <h3 className="logo">Client 1</h3>
           <div className="page">
                <h1 className="h1">Sign In</h1>
                {ApiError &&
                    <div className="alert alertRed">
                        <h3 className="h3">{ApiError}</h3>
                    </div>
                }
                <LoginForm 
                    formStruct={formStruct}
                    submit = {onSubmit}
                    errByApi= {onErrByApi}
                />
           </div>
        </div>
    );
};

export default connect(null, {login})(Client1);