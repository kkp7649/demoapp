import axios from "axios";

export default {
	user : {
		login : credentials=> 
			axios.post("/forNadeem/client1Login", {credentials} ).then(res => res.data),
		getUserData : jwt=> 
			axios.post("/forNadeem/client1GetUserData", {jwt} ).then(res => res.data),
	}
}