import {CLIENT1_LOGGED_IN} from "./types";

export default function Client1ScreenUser(state = {}, action = {}) {
	switch (action.type) {
		case CLIENT1_LOGGED_IN:
			return action.user;
		default:
			return state
	}
}