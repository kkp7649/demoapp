import {CLIENT2_LOGGED_IN} from "./types";

export default function Client2ScreenUser(state = {}, action = {}) {
	switch (action.type) {
		case CLIENT2_LOGGED_IN:
			return action.user;
		default:
			return state
	}
}