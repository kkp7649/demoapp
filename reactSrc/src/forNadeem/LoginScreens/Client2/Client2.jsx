import React, {useState} from "react";
import {connect} from "react-redux";
import {login} from "./AuthAction";
import LoginForm from "../LoginForm";

const Client1 = (props) => {
    const [ApiError, setApiError] = useState("");
    const formStruct = {
        field1: {
            name: 'username',
            label: 'Username: ',
            type: 'text',
            placeholder: 'Enter the username'
        },
        field2: {
            name: 'password',
            label: 'Password: ',
            type: 'password',
            placeholder: 'Enter the Password'
        },
        cta: {
            text: 'Login in',
            color: 'Green'
        }
    };
    const onSubmit = (data) => 
        props.login(data).then(msg => {
            setApiError("");
            console.log(msg)
            props.history.push("/formAssignment/client2/dashboard/")
        });
    
    const onErrByApi = (msg) => {
        console.log(msg.response.data)
        setApiError(msg.response.data.err.errors.global);
    }
    return(
        <div>
           <h3 className="logo">Client 2</h3>
           <div className="page">
                <h1 className="h1">Login Please</h1>
                {ApiError &&
                    <div className="alert alertRed">
                        <h3 className="h3">{ApiError}</h3>
                    </div>
                }
                <LoginForm 
                    formStruct={formStruct}
                    submit = {onSubmit}
                    errByApi= {onErrByApi}
                />
           </div>
        </div>
    );
};

export default connect(null, {login})(Client1);