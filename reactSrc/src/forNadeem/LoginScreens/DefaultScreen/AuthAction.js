import ApiCalls from "./ApiCalls"; 
import { USER_LOGGED_IN } from './types';
import React from 'react';

export const userLoggedIn = (user) => ({
	type: USER_LOGGED_IN,
	user
});
	

export const login = (credentials) => dispatch =>  
	ApiCalls.user.login(credentials).then((user) => {
		localStorage.DefaultScreenJWT = user.token;
		dispatch(userLoggedIn(user.userData))
		return user.user;
	});

export const getUserData = (jwt) => dispatch =>  
	ApiCalls.user.getUserData(jwt).then((user) => {
		dispatch(userLoggedIn(user))
		return user;
	});
// iota@smile.com
// Bhaiya ab yadi coding kar rahe ho to yadi abhi hum chat pe hai to mujhe bula lena
//"https://www.youtube.com/watch?v=DfBIawJ6dVU&list=PLG3RxIUKLJlbDDGeeoUCkinS2DUybp_1o&index=7"
//https://200skyfiregce-vimeo.akamaized.net/exp=1574076685~acl=%2F318716599%2F%2A~hmac=7b1fbe0ba1aad212be7713e69c076453420755174d30d7487d7b5ef7cedeb9db/318716599/sep/video/1235327740/chop/segment-1.m4s
//