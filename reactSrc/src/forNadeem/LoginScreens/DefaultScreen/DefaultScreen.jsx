import React, {useState} from "react";
import {connect} from "react-redux";
import {login} from "./AuthAction";
import LoginForm from "../LoginForm";

const DefaultScreen = (props) => {
    const [ApiError, setApiError] = useState("");
    const formStruct = {
        field1: {
            name: 'email',
            label: 'Username: ',
            type: 'email',
            placeholder: 'Use your email as username'
        },
        field2: {
            name: 'password',
            label: 'Password: ',
            type: 'password',
            placeholder: 'Enter your Password'
        },
        cta: {
            text: 'Sign in',
            color: 'Blue'
        }
    };
    const onSubmit = (data) => 
        props.login(data).then(msg => {
            setApiError("");
            console.log(msg)
            props.history.push("/formAssignment/default/dashboard/")
        });
    
    const onErrByApi = (msg) => {
        setApiError(msg.response.data.err.errors.global);
        // console.log(msg)
    }
    return(
        <div>
           <h3 className="logo">Default</h3>
           <div className="page">
                <h1 className="h1">Login</h1>
                {ApiError &&
                    <div className="alert alertRed">
                        <h3 className="h3">{ApiError}</h3>
                    </div>
                }
                <LoginForm 
                    formStruct={formStruct}
                    submit = {onSubmit}
                    errByApi= {onErrByApi}
                />
           </div>
        </div>
    );
};

export default connect(null, {login})(DefaultScreen);