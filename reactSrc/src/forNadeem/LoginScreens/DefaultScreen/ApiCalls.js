import axios from "axios";

export default {
	user : {
		login : credentials=> 
			axios.post("/forNadeem/login", {credentials} ).then(res => res.data),
		register: data=>
			axios.post("api/new_user", {data} ).then(res=> res.data.user),
		getUserData : jwt=> 
			axios.post("/forNadeem/getUserData", {jwt} ).then(res => res.data),
	}
}