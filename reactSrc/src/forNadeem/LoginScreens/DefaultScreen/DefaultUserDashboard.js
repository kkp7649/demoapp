import React from "react";
import {getUserData} from "./AuthAction";
import {connect} from "react-redux";

class UserDashboard extends React.Component{
    state={
        userData: {},
        ApiError: ""
    }
    componentDidMount(){
        if(localStorage.DefaultScreenJWT){
            if(!this.props.user.name){
                this.props.getUserData(localStorage.DefaultScreenJWT).then(data => {
                    this.setState({userData: data});
                }).catch(msg => {
                    this.setState({ApiError: msg.response.data.err.errors.global});
                });
            }
        }
    }
    render(){
        const {userData, ApiError} = this.state;
        const {user} = this.props;
        return(
            <div>
               <h3 className="logo">Default</h3>
                <div className="page">
                   {ApiError && 
                        <div className="alert alertRed">
                            <h3 className="h3">{ApiError}</h3>
                        </div>
                   }
                   {user.name &&
                        <div>
                            <h1 className="h1">Welcome Back, {user.name}</h1>
                            <h4 className="h4">As per our records your details are as under:</h4>
                            <h3 className="h3">Name: {user.name}</h3>
                            <h3 className="h3">Email: {user.email}</h3>
                        </div>
                   }
                   {!ApiError && !user.name && 
                        <div className="loader">
                            <div></div>
                            <h1>Please Wait...</h1>
                        </div>
                   }
                </div> 
            </div>
        );
    }
};


function mapStateToProps (state) {
    return {user: state.DefaultScreenUser}
}

export default connect(mapStateToProps, {getUserData})(UserDashboard);