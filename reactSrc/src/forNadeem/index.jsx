import React from "react";
import {Link} from "react-router-dom";

const FormAssignment = () => {
    return(
        <div className="page">
            <h1 className="h1">Welcome to the App</h1>
            <h3 className="h3">The app consits of login screen for different clients.</h3>
            <h3 className="h3">Click on the login screen you want to see.</h3>
            <Link to="/formAssignment/default/login" className="list py-0">
                <h3 className="textBlue">Default Login Screen</h3>
            </Link>
            <Link to="/formAssignment/client1/login" className="list py-0">
                <h3 className="textBlue">Login Screen for Client 1</h3>
            </Link>
            <Link to="/formAssignment/client2/login" className="list py-0">
                <h3 className="textBlue">Login Screen for Client 2</h3>
            </Link>
            <Link to="/formAssignment/client3/login" className="list py-0">
                <h3 className="textBlue">Login Screen for Client 3</h3>
            </Link>
        </div>
    );
};

export default FormAssignment;