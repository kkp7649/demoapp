import { combineReducers } from 'redux';
import DefaultScreenUser from "./forNadeem/LoginScreens/DefaultScreen/UserReducer";
import Client1ScreenUser from "./forNadeem/LoginScreens/Client1/UserReducer";
import Client2ScreenUser from "./forNadeem/LoginScreens/Client2/UserReducer";
import Client3ScreenUser from "./forNadeem/LoginScreens/Client3/UserReducer";
export default combineReducers ({
	DefaultScreenUser,
	Client1ScreenUser,
	Client2ScreenUser,
	Client3ScreenUser
});