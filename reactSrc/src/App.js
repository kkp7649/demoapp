import React from 'react';
import {Route} from 'react-router-dom';
import DefaultScreen from "./forNadeem/LoginScreens/DefaultScreen/DefaultScreen";
import Client1 from "./forNadeem/LoginScreens/Client1/Client1";
import Client2 from "./forNadeem/LoginScreens/Client2/Client2";
import Client3 from "./forNadeem/LoginScreens/Client3/Client3";
import DefaultUserDashboard from "./forNadeem/LoginScreens/DefaultScreen/DefaultUserDashboard";
import Client1UserDashboard from "./forNadeem/LoginScreens/Client1/Client1UserDashboard";
import Client2UserDashboard from "./forNadeem/LoginScreens/Client2/Client2UserDashboard";
import Client3UserDashboard from "./forNadeem/LoginScreens/Client3/Client3UserDashboard";
import LandingPage from "./components/LandingPage";
import FormAssignment from "./forNadeem/";
import DynamicForm from "./components/dynamicForm/";

function App( {location} ) {
  return (
    <div>
      <Route path = '/' exact component = {LandingPage} />
      <Route path = '/formAssignment/' exact component = {FormAssignment} />
      <Route path = '/dynamicForm' exact component = {DynamicForm} />
      <Route path = '/formAssignment/default/login' exact component = {DefaultScreen} />
      <Route path = '/formAssignment/default/dashboard' exact component = {DefaultUserDashboard} />
      <Route path = '/formAssignment/client1/login' exact component = {Client1} />
      <Route path = '/formAssignment/client1/Dashboard/' exact component = {Client1UserDashboard} />
      <Route path = '/formAssignment/client2/login' exact component = {Client2} />
      <Route path = '/formAssignment/client2/Dashboard/' exact component = {Client2UserDashboard} />
      <Route path = '/formAssignment/client3/login' exact component = {Client3} />
      <Route path = '/formAssignment/client3/Dashboard/' exact component = {Client3UserDashboard} />
    </div>
  );
}

export default App;

