import React from "react";
import phoneCC from "./phoneCC";
class DynamicForm extends React.Component{
    state = {
        data: {
            countryCode: 0,
            phoneNo: ''
        },
        errors: {
            'phoneNumber': ""
        }
    }
    onChange = event => this.setState({
        data: {...this.state.data, [event.target.name] : event.target.value }
    });
    onSubmit=(e) => {
        e.preventDefault();
        var toSend = {
            ...this.state.data
        };
        toSend.phoneNumber = toSend.countryCode + " "+toSend.phoneNo;
        if(this.state.data.phoneNo!==''){
            if(this.state.data.countryCode !== 0){
                this.props.submit(toSend);
                this.setState({errors: {...this.state.errors, phoneNumber: ""}});
            }else{
                this.setState({errors: {...this.state.errors, phoneNumber: "Please Select Your Country"}})
            }
        }else{
            this.props.submit(toSend);
            this.setState({errors: {...this.state.errors, phoneNumber: ""}});
        }
    }
    renderForm = () => {

        const {model} = this.props;
        const {onChange} = this;
        let formUI = model.map((m) => {
            let key = m.key;
            let type = m.type;
            let props = m.props;            
            if(type === 'textarea') {
                 return (<div className="form-group" key={key}>
                        <label htmlFor="message">{m.label}</label>
                        <textarea
                            className="form-control" 
                            {...props}
                            id={key} 
                            name={key}
                            onChange={onChange}
                        ></textarea>
                    </div>);
            }else if (type === 'select' ) {
                return (
                    <div className="form-group" key={key} >
                        <label htmlFor={key}>{m.label} </label>
                        <select 
                            className="form-control" 
                            id={key} 
                            name={key}
                            onChange={onChange}
                        >
                        { m.options.map((option) => {

                           return <option {...option.props} className="form-control" value={option.value}>{option.label}</option>;
                        })
                        }
                        </select>
                    </div>);
            }else if(type === 'phoneNo') {
                const {phoneNumber} = this.state.errors;
                return (
                    <div className="form-group">
                        <label htmlFor="phoneNo">{m.label}</label><br/>
                        <select name="countryCode" id="countryCode" className="form-control countryCode" onChange={onChange}><br/>
                            <option value="">Country Code</option>
                            {phoneCC.map((phoneCC) => {
                             return <option value={phoneCC.dial_code} className="form-control">
                                {phoneCC.code} {phoneCC.dial_code}  {phoneCC.name}
                            </option>;

                            })
                            }
                        </select>
                        <input type="number" className="form-control phoneNumber" onChange={onChange} name="phoneNo" id="phoneNo" />
                        {phoneNumber && <span style={{'color': '#f00'}} className="h3">{phoneNumber}</span>}
                    </div>
                );
            }else if(type === 'checkbox') {
                return (
                    <div className="form-group checkbox">
                        <input type="checkbox" onChange={onChange} name={key} id={key} />
                        <label htmlFor={key}>{m.label}</label><br/>
                    </div>
                );
            }else{
                  return (
                    <div className="form-group" key = {key}>
                        <label htmlFor={key}>{m.label} </label>
                        <input 
                            type={type}
                            className="form-control" 
                            {...props}
                            id={key} 
                            name={key}
                            onChange={onChange}
                        />
                    </div>
                ); 
            }


            
        });
        return formUI;
    }
    render(){
        const {renderForm, onSubmit} = this;
        return(
            <div>
                <h3 className="h3">{this.props.formTitle}</h3>
                <form onSubmit = {onSubmit}>
                    {renderForm()}
                    <input type="submit" value={this.props.btnCTA} className="btn btnGreen btnright" />
                </form>
            </div>
        );
    };
};

export default DynamicForm;