import React from 'react'; 
import {Link} from "react-router-dom";

const LandingPage = () => {
	return(
		<div className="page">
			<h1 className="h1">Welcome to Demo App</h1>
			<h3 className="h3">This app is made for doing demos of simple apps or components
			 along with any assignments given by someone.</h3>
			 <h4 className="h4">Click on the below link to visit the given app</h4>
			 <Link to="/formAssignment" className="list">
			 	<h3>Form Assignment for Nadeem</h3>
			 	<div className="d-flex">
			 		<Link to="/formAssignment" className="btn btnBlue">Visit App</Link>
			 	</div>
			 </Link>
		</div>
	);
};


export default LandingPage; 