import express from "express";
import user from "./db/user.js";
import client1 from "./db/client1.js";
import client2 from "./db/client2.js";
import client3 from "./db/client3.js";
const router = express.Router();

router.post("/login", (req, res) => {
    const {credentials} = req.body;
    console.log(credentials); 
    user.login(credentials)
        .then((data) => {
            res.json(data);
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/getUserData", (req, res) => {
    const {jwt} = req.body;
    console.log(jwt); 
    user.getUserData(jwt)
        .then((data) => {
            res.json(data);
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client1GetUserData", (req, res) => {
    const {jwt} = req.body;
    console.log(jwt); 
    client1.client1GetUserData(jwt)
        .then((data) => {
            res.json(data);
            console.log(data)
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client1Login", (req, res) => {
    const {credentials} = req.body;
    console.log(credentials); 
    client1.client1Login(credentials)
        .then((data) => {
            res.json(data);
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client2GetUserData", (req, res) => {
    const {jwt} = req.body;
    console.log(jwt); 
    client2.client2GetUserData(jwt)
        .then((data) => {
            res.json(data);
            console.log(data)
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client2Login", (req, res) => {
    const {credentials} = req.body;
    console.log(credentials); 
    client2.client2Login(credentials)
        .then((data) => {
            res.json(data);
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client3GetUserData", (req, res) => {
    const {jwt} = req.body;
    console.log(jwt); 
    client3.client3GetUserData(jwt)
        .then((data) => {
            res.json(data);
            console.log(data)
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

router.post("/client3Login", (req, res) => {
    const {credentials} = req.body;
    console.log(credentials); 
    client3.client3Login(credentials)
        .then((data) => {
            res.json(data);
        }).catch(err => {
            console.log(err);
            res.status(400).json({err});
        });
});

export default router; 