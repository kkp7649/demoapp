import jwt from "jsonwebtoken";

function generateJWT(data) {
    return jwt.sign({ data: data }, "key");
}

const userData = {
    name: 'Nadeem Qureshi',
    password: 'pass',
    email: 'nadeem@gmail.com',
    username: 'nadeem1234'
}

export default {
    client2Login: (data) => client2Login(data),
    client2GetUserData: (data) => client2GetUserData(data)
}

function client2Login(data) {
    return new Promise((resolve, reject) => {
        if (data.username === userData.username) {
            if (data.password === userData.password) {
                resolve({
                    userData: {
                        name: 'Nadeem Qureshi',
                        email: 'nadeem@gmail.com',
                        username: 'nadeem1234'
                    },
                    token: generateJWT('nadeem@gmail.com')
                })
            } else {
                reject({ errors: { global: "Incorrect password" } })
            }
        } else {
            reject({ errors: { global: "Invalid Username" } })
        }
    });
}

function client2GetUserData(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, 'key', function(jwterr, decoded) {
            if (!jwterr) {
                resolve({
                    name: 'Nadeem Qureshi',
                    password: 'pass',
                    email: 'nadeem@gmail.com',
                    username: 'nadeem1234'
                })
            } else {
                reject({ errors: { global: "Session Expired Please Signin again" } });
            }
        });
    });
}