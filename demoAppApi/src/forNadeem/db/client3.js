import jwt from "jsonwebtoken";

function generateJWT(data) {
    return jwt.sign({ data: data }, "key");
}

const userData = {
    name: 'Nadeem Qureshi',
    uid: '1234',
    password: 'pass',
    email: 'nadeem@gmail.com',
    username: 'nadeem1234'
}

export default {
    client3Login: (data) => client3Login(data),
    client3GetUserData: (data) => client3GetUserData(data)
}

function client3Login(data) {
    return new Promise((resolve, reject) => {
        if (data.uid === userData.uid) {
            if (data.password === userData.password) {
                resolve({
                    userData: {
                        name: 'Nadeem Qureshi',
                        email: 'nadeem@gmail.com',
                        username: 'nadeem1234'
                    },
                    token: generateJWT('nadeem@gmail.com')
                })
            } else {
                reject({ errors: { global: "Incorrect Password" } })
            }
        } else {
            reject({ errors: { global: "Invalid Unique Id" } })
        }
    });
}

function client3GetUserData(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, 'key', function(jwterr, decoded) {
            if (!jwterr) {
                resolve({
                    name: 'Nadeem Qureshi',
                    email: 'nadeem@gmail.com',
                    username: 'nadeem1234'
                })
            } else {
                reject({ errors: { global: "Session Expired Please Signin again" } });
            }
        });
    });
}