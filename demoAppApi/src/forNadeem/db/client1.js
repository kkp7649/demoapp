import jwt from "jsonwebtoken";

function generateJWT(data) {
    return jwt.sign({ data: data }, "key");
}

const userData = {
    acname: 'Nadeem Qureshi',
    key: 'securitykey',
    email: 'nadeem@gmail.com',
    username: 'nadeem1234'
}

export default {
    client1Login: (data) => client1Login(data),
    client1GetUserData: (data) => client1GetUserData(data)
}

function client1Login(data) {
    return new Promise((resolve, reject) => {
        if (data.acname === 'Nadeem Qureshi') {
            if (data.key === 'securitykey') {
                resolve({ userData: userData, token: generateJWT('nadeem@gmail.com') })
            } else {
                reject({ errors: { global: "Incorrect Key" } })
            }
        } else {
            reject({ errors: { global: "Invalid Account Name" } })
        }
    });
}

function client1GetUserData(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, 'key', function(jwterr, decoded) {
            if (!jwterr) {
                resolve({
                    acname: 'Nadeem Qureshi',
                    email: 'nadeem@gmail.com',
                    username: 'nadeem1234'
                })
            } else {
                reject({ errors: { global: "Session Expired Please Signin again" } });
            }
        });
    });
}