const MongoClient = require('mongodb').MongoClient;
import assert from "assert";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
const url = "mongodb://localhost:27017";
const db_name = "demoAppDb";

export default{
    login: (data) => login(data),
    getUserData: (data) => getUserData(data)
}

function generateJWT(data) {
    return jwt.sign({data: data}, "key");
}

const login = (data) => {
    const {email, password} = data;
    const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true  });
    return new Promise((resolve, reject) => {

        client.connect(err => {
            if(!err){
            console.log("Connected correctly to server");
            const collection = client.db("demoAppDb").collection("users");
            collection.findOne({"email": email}, (err, result) => {     
                console.log("Finding");           
                if (result) {
                    console.log("Found");           
                    console.log(result);           
                    bcrypt.compare(password, result.password, (err, resultt) => { 
                        console.log("Comparing");           
                        if(resultt === true){
                            console.log("Compared");        
                            resolve({userData: {name: result.name, email: result.email}, token:email});
                        }else{
                            reject({errors: {global: "Incorrect Password"}})
                        }
                    })
                }else{
                    console.log(err);
                    reject({errors: {global:"Sorry the email you have provided is incorrect."}});                 
                }
            });
            }else{
                reject({errors: {global:"Internal Server Error Please contact the administrator"}});
            } 
        })
        
    })
}

const getUserData = (token) => {
    const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true  });
    return new Promise((resolve, reject) => {
        jwt.verify(token, 'key', function(jwterr, decoded){
            if(!jwterr){
              client.connect(err => {
                if(!err){
                    console.log("Connected correctly to server");
                    const email = decoded.data;
                    console.log(email);
                    const collection = client.db("demoAppDb").collection("users");
                    collection.findOne({"email": email}, (err, result) => {
                        if(result){
                            console.log(result);
                            console.log(err);
                            resolve({name: result.name, email: result.email});
                        }else{
                            reject({errors: {global:"Internal Server Error Please contact the administrator"}});
                        }
                    });
                    }else{
                        reject({errors: {global:"Internal Server Error Please contact the administrator"}});
                    } 
                })  
            }else{
                reject({errors: {global:"Session Expired Please Signin again"}});
            }
        })
        
        
    })
}