import express from "express";
import path from "path";
import cors from "cors";
import auth from "./routes/auth";
import newUser from "./routes/new_user";
import forNadeem from "./forNadeem/forNadeem";
import bodyParser from "body-parser";


const app = express(); 
const port = "8080";

app.use(cors({credentials: true, origin: ['http://localhost:3000']}));
app.use(bodyParser.json());

app.use("/forNadeem/", forNadeem);


app.get("/", (req, res) => {
	res.sendFile(path.join( __dirname, "index.html"))
})

app.listen(port, () => console.log(`Server listening on port: ${port} `))